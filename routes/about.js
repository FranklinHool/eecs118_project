/**
 * Created by Franklin on 3/5/2016.
 */
var express = require('express');
var router = express.Router();

/* GET about page. */
router.get('/', function(req, res, next) {
  res.render('about', { title: 'ABOUT' });
});

module.exports = router;