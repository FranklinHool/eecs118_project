/**
 * Created by Franklin on 2/14/2016.
 */


// After the API loads, call a function to get the uploads playlist ID.
function handleAPILoaded() {
    $('#search-button').attr('disabled', false);
}


function search() {
    //console.log("CLICKED");


    var table = document.getElementById("result_table");
    var header = table.createTHead();


    var row = header.insertRow(0);
    var cell = row.insertCell(0);
    cell.innerHTML = "<b>Link</b>";
    var cell = row.insertCell(0);
    cell.innerHTML = "<b>Video</b>";
    var cell = row.insertCell(0);
    cell.innerHTML = "<b>Description</b>";
    var cell = row.insertCell(0);
    cell.innerHTML = "<b>Title</b>";

    var list = [];

    //Make the initial youtube search response for a list of results
    var q = $('#query').val();
    var request = gapi.client.youtube.search.list({
        q: q,
        part: 'snippet',
        maxResults: 50

    });

    var t = $('#title').val();
    var request_2 = gapi.client.youtube.search.list({
        q: t,
        part: 'snippet',
        maxResults: 50

    });


    //Parses all the results and tries to find the best result by splitting the query into predicates
    request.execute(function (response) {
        //console.log(JSON.stringify(response));
        var link = null;
        var strWords = q.split(" ");
        var map = {};


        for (var words in strWords) {
            map[words] = 0;
        }
        var count = 0;
        for (var t = 0; t < JSON.stringify(response.pageInfo.resultsPerPage); t++) {
            //console.log(JSON.stringify(response.items[t].snippet.title));
            for (var word4 in strWords) {
                if (JSON.stringify(response.items[t].snippet.title).indexOf(word4) > -1) {
                    map[word4] = 1;
                }
            }
            var num1 = 0;
            for (var word3 in strWords) {
                if (map[word3] == 1) {
                    num1++;
                }
                //console.log(num1);
            }
            if (num1 == strWords.length) {
                link = JSON.stringify(response.items[t].id.videoId);
                list.push(link);
            }
        }
        while (nextPage(JSON.stringify(response.nextPageToken).replace(/["]/g, "")) != null) {
            for (var i = 0; i < parseInt(JSON.stringify(response.pageInfo.resultsPerPage)); i++) {
                var str2 = (JSON.stringify(response.result));
                for (var y = 0; y < parseInt(JSON.stringify(str2.pageInfo.resultsPerPage)); y++) {
                    for (var word in strWords) {
                        if (JSON.stringify(str2.items[y].snippet.title).indexOf(word) > -1) {
                            map[word] = 1;
                        }
                    }
                    var num = 0;
                    for (var word2 in strWords) {
                        if (map[word2] == 1) {
                            num++;
                        }
                    }
                    if (num == strWords.length) {
                        link = JSON.stringify(str2.items[y].id.videoId);
                        list.push(link);
                    }
                }
            }
        }
        // console.log("BEFORE");
        //console.log(list.toString());
        //$('#search-container').html('<iframe width = "840" height = "630" src="http:////www.youtube.com//embed//' + link.replace(/["]/g, "") + '"></iframe>');


    });
    var temp = [];
    var temp2 = [];

    var titles = [];
    var titles2 = [];
    var descriptions = [];
    var descriptions2 = [];
    request_2.execute(function (response) {

        var link = null;
        var title = null;
        var strWords = q.split(" ");
        var map = {};


        for (var words in strWords) {
            map[words] = 0;
        }

        for (var t = 0; t < JSON.stringify(response.pageInfo.resultsPerPage); t++) {
            //console.log(JSON.stringify(response.items[t].snippet.title));
            for (var word4 in strWords) {
                if (JSON.stringify(response.items[t].snippet.description).indexOf(word4) > -1) {
                    map[word4] = 1;
                }
            }
            var num1 = 0;
            for (var word3 in strWords) {
                if (map[word3] == 1) {
                    num1++;
                }
                //console.log(num1);
            }
            if (num1 == strWords.length) {
                link = JSON.stringify(response.items[t].id.videoId);
                temp.push(link);
                titles.push(JSON.stringify((response.items[t].snippet.title)));
                descriptions.push(JSON.stringify(response.items[t].snippet.description));

            }
        }

        var tlist = list;
        //console.log(tlist[0] + " "+ list[0]);
        console.log(list[0] + " " + titles[0] + " " + descriptions[0]);
        for (var k = 0; k < temp.length; k++) {
            if (list.indexOf(temp[k]) == -1) {
                temp2.push(list.splice(list.indexOf(temp[k])));
                console.log(list.indexOf(temp[k]));
                titles2.push(titles.splice(titles.indexOf(temp[k])));
                descriptions2.push(descriptions.splice(descriptions.indexOf(temp[k])));
            }
        }
        //console.log("TITLES" + titles2.toString());
        //console.log("Descriptions" + descriptions2.toString());
        //console.log("Links" + temp2.toString());
        //$('#search-container').html('<iframe width = "840" height = "630" src="http:////www.youtube.com//embed//' + link.replace(/["]/g, "") + '"></iframe>');

        var num = $('#results').val();
        if (num == "") {
            num = 20;
        }
        //console.log(temp2.toString().split(",")[0]);
        for (var f1 = 0; f1 < num; f1++) {

            //console.log("DO I GET HERE?");
            var row = header.insertRow(-1);
            var cell = row.insertCell(-1);
            cell.innerHTML = titles2[f1];
            var cell = row.insertCell(-1);
            cell.innerHTML = descriptions2[f1];
            var cell = row.insertCell(-1);
            cell.innerHTML = '<iframe width = "200" height = "200" src="http:////www.youtube.com//embed//' + temp2.toString().split(",")[f1].replace(/["]/g, "") + '"></iframe>';
            var cell = row.insertCell(-1);
            cell.innerHTML = '<a href = "http:////www.youtube.com/watch/?v=' + temp2.toString().split(",")[f1].replace(/["]/g, "") + '">link</a>';
        }
    });


}
// Create a listing for a video.
function displayResult(videoSnippet) {
    var title = videoSnippet.title;
    var videoId = videoSnippet.resourceId.videoId;


    $('#video-container').append('<p>' + title + ' - ' + videoId + '</p>');
}

// Retrieve the next page of videos in the playlist.
function nextPage(q, nextPageToken) {
    var request = gapi.client.youtube.search.list({
        q: q,
        part: 'snippet',
        maxResults: 50,
        pageToken: nextPageToken

    });

    request.execute(function (response) {
        //console.log(JSON.stringify(response.result))
        return JSON.stringify(response.result)

    });
}
